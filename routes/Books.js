const express = require("express")
const books = express.Router()
const cors = require("cors")

const Book = require("../models/Book")

books.use(cors())

/* Add Book */
books.post("/createBook", (req, res) => {
    const today = new Date()
    const bookData = {
        book_title: req.body.book_title,
        book_author: req.body.book_author,
        book_NumOfCop: req.body.book_NumOfCop,
        description: req.body.description,
        created: today
    }

    Book.findOne({
        book_title: req.body.book_title
    })
    .then(book => {
    if (!book) {
        Book.create(bookData)
            .then(book => {
                res.json({ status: book.book_title + ' is created',bookID: book._id})
            })
            .catch(err => {
                res.send('error: ' + err)
            })
    } else {
        res.json({ error: 'Book already exists' })
    }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/* Get Book one */
books.get("/queryBook/:book_id", (req, res) => {
    console.log(req.params.book_id)
    Book.findOne({
        _id: req.params.book_id
        //this.$route.query.userID
    })
    .then(book => {
        const qBook = {
            _id: book._id,
            book_title: book.book_title,
            book_author: book.book_author,
            book_NumOfCop: book.book_NumOfCop,
            description: book.description,
        }
        res.send(qBook)
    })
})

/* Get Book list */
books.get("/all", (req, res) => {
    Book.find({})
        .sort({ update_at: -1 })
        .then(books => {
            setTimeout(() =>{
             res.json(books);
            },1000)
        })
        .catch(err => {
            console.log(2);
            res.json(err);
        });
})

/* Update Book Information*/
books.put("/Updatebook/:book_id", (req, res) => {
    Book.findOneAndUpdate({
        _id: req.params.book_id
    },{
        $set: {
            book_title: req.body.book_title,
            book_author: req.body.book_author,
            book_NumOfCop: req.body.book_NumOfCop,
            description: req.body.description,
        }
    },
    {
        new: true
    }
    ).then(book => 
        setTimeout(() =>{
            res.json(book);
           },700)
        )
     .catch(err => res.json(err))
})

/* Delete a Book */
books.delete("/delBookbyID/:book_id", (req, res) => {
    Book.findOneAndRemove({
      _id: req.params.book_id
    })
      .then(book => res.send(`${book.book_title} Successfully deleted`))
      .catch(err => res.json(err));
});


module.exports = books