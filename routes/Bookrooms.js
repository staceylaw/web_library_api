const express = require("express")
const bookrooms = express.Router()
const cors = require("cors")

const Bookroom = require("../models/Bookroom")

bookrooms.use(cors())

/* Add bookrooms */
bookrooms.post("/createBookroom", (req, res) => {
    const today = new Date()
    const bookroomData = {
        user_id: req.body.user_id,
        user_name: req.body.user_name,
        booking_email: req.body.booking_email,
        booking_tel: req.body.booking_tel,
        booking_room: req.body.booking_room,
        booking_Usage: req.body.booking_Usage,
        booking_Date: req.body.booking_Date,
        created: today
    }

    Bookroom.findOne({
        user_id: req.body.user_id
    })
    .then(bookroom => {
    if (!bookroom) {
        Bookroom.create(bookroomData)
            .then(bookroom => {
                res.json({ status: bookroom.booking_email + ' is created',bookroomID: bookroom._id})
            })
            .catch(err => {
                res.send('error: ' + err)
            })
    } else {
        res.json({ error: 'Book already exists' })
    }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/* Get bookrooms list */
bookrooms.get("/all", (req, res) => {
    Bookroom.find({})
        .sort({ update_at: -1 })
        .then(bookrooms => {
            setTimeout(() =>{
             res.json(bookrooms);
            },1000)
        })
        .catch(err => {
            console.log(2);
            res.json(err);
        });
})

/* Get bookrooms one */
bookrooms.get("/queryBookroom/:bookroom_id", (req, res) => {
    console.log(req.params.bookroom_id)
    Bookroom.findOne({
        _id: req.params.bookroom_id
    })
    .then(bookroom => {
        const qBookrooms = {
            _id: bookroom._id,
            user_id: bookroom.user_id,
            user_name: bookroom.user_name,
            booking_email: bookroom.booking_email,
            booking_tel: bookroom.booking_tel,
            booking_room: bookroom.booking_room,
            booking_Usage: bookroom.booking_Usage,
            booking_Date: bookroom.booking_Date
        }
        res.send(qBookrooms)
    })
})

/* Update bookrooms */
bookrooms.put("/UpdateBookroom/:bookroom_id", (req, res) => {
    Bookroom.findOneAndUpdate({
        _id: req.params.bookroom_id
    },{
        $set: {
            user_id: req.body.user_id,
            user_name: req.body.user_name,
            booking_email: req.body.booking_email,
            booking_tel: req.body.booking_tel,
            booking_room: req.body.booking_room,
            booking_Usage: req.body.booking_Usage,
            booking_Date: req.body.booking_Date
        }
    },
    {
        new: true
    }
    ).then(bookroom => 
        setTimeout(() =>{
            res.json(bookroom);
           },700)
        )
     .catch(err => res.json(err))
})

/* Delete a bookrooms */
bookrooms.delete("/delbookroombyID/:Bookroom_id", (req, res) => {
    Bookroom.findOneAndRemove({
      _id: req.params.Bookroom_id
    })
      .then(bookrooms => res.send(`${bookrooms.booking_email} Successfully deleted`))
      .catch(err => res.json(err));
});

module.exports = bookrooms