const express = require("express")
const loans = express.Router()
const cors = require("cors")

const Loan = require("../models/Loan")
loans.use(cors())

/* Add Loan */
loans.post("/createLoan", (req, res) => {
    const today = new Date()
    const loanData = {
        book_id: req.body.book_id,
        book_title: req.body.book_title,
        user_id: req.body.user_id,
        user_name: req.body.user_name,
        loan_Issue_Date: req.body.loan_Issue_Date,
        loan_Due_Date: req.body.loan_Due_Date,
        loan_Status: req.body.loan_Status,
        created: today
    }

    Loan.findOne({
        book_title: req.body.book_title
    })
    .then(loan => {
    if (!loan) {
        Loan.create(loanData)
            .then(loan => {
                res.json({ status: loan.book_title + ' is created',loanID: loan._id})
            })
            .catch(err => {
                res.send('error: ' + err)
            })
    } else {
        res.json({ error: 'Loan already exists' })
    }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})


/* Get Loan list */
loans.get("/all", (req, res) => {
    Loan.find({})
        .sort({ update_at: -1 })
        .then(loans => {
            setTimeout(() =>{
             res.json(loans);
            },1000)
        })
        .catch(err => {
            console.log(2);
            res.json(err);
        });
})

/* Get Loan one */
loans.get("/queryLoan/:loan_id", (req, res) => {
    console.log(req.params.loan_id)
    Loan.findOne({
        _id: req.params.loan_id
        //this.$route.query.userID
    })
    .then(loan => {
        const qLoan = {
            _id: loan._id,
            book_id: loan.book_id,
            book_title: loan.book_title,
            user_id: loan.user_id,
            user_name: loan.user_name,
            loan_Issue_Date: loan.loan_Issue_Date,
            loan_Due_Date: loan.loan_Due_Date,
            loan_Status: loan.loan_Status,
        }
        res.send(qLoan)
    })
})

/* Update Loan */
loans.put("/UpdateLoan/:loan_id", (req, res) => {
    Loan.findOneAndUpdate({
        _id: req.params.loan_id
    },{
        $set: {
            book_id: req.body.book_id,
            book_title: req.body.book_title,
            user_id: req.body.user_id,
            user_name: req.body.user_name,
            loan_Issue_Date: req.body.loan_Issue_Date,
            loan_Due_Date: req.body.loan_Due_Date,
            loan_Status: req.body.loan_Status,
        }
    },
    {
        new: true
    }
    ).then(loan => 
        setTimeout(() =>{
            res.json(loan);
           },700)
        )
     .catch(err => res.json(err))
})

/* Delete a Loan */
loans.delete("/delloanbyID/:loan_id", (req, res) => {
    Loan.findOneAndRemove({
      _id: req.params.loan_id
    })
      .then(loan => res.send(`${loan.book_title} Successfully deleted`))
      .catch(err => res.json(err));
});



module.exports = loans