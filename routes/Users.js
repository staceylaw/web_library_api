const express = require("express")
const users = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const User = require("../models/User")
users.use(cors())

process.env.SECRET_KEY = 'secret'

/* Add User register */
users.post("/register", (req, res) => {
    const today = new Date()
    const userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        type: "member",
        created: today
    }

    User.findOne({
        email: req.body.email
    })
    .then(user => {
        if (!user) {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                userData.password = hash
                User.create(userData)
                    .then(user => {
                        // res.status(201).json({ status: user.email + ' registered', userID: user._id, message: 'User created!' })
                        res.json({ status: user.email + ' registered',})
                    })
                    .catch(err => {
                        res.send('error: ' + err)
                    })
            })
        } else {
            res.json({ error: 'User already exists' })
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/* post User login */
users.post('/login', (req, res) => {
    User.findOne({
        email: req.body.email
    })
    .then(user => {
        if (user) {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                const payload = {
                    _id: user._id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    
                }
                let token = jwt.sign(payload, process.env.SECRET_KEY, {
                    expiresIn: 1440
                })
                res.send(token)
            } else {
                res.json({ error: 'Incorrect account or password' })
            }
        } else {
            res.json({ error: 'Incorrect account or password' })
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/* Get User profile */
users.get('/profile', (req, res) => {
    var decoded = jwt.verify(req.headers["authorization"],process.env.SECRET_KEY)
    
    User.findOne({
        id: decoded._id
    })
    .then(user => {
        if (user) {
            res.json(user)
        }else{
            res.send('User dose not exist')
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

/* Get User one */
users.get("/queryUser/:id", (req, res) => {
    console.log(req.params.id)
    User.findOne({
        _id: req.params.id
        //this.$route.query.userID
    })
    .then(user => {
        const qUser = {
            _id: user._id,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
        }
        res.send(qUser)
    })
})

/* Get User list */
users.get("/all", (req, res) => {
    User.find({})
        .sort({ update_at: -1 })
        .then(users => {
            setTimeout(() =>{
             res.json(users);
            },1000)
        })
        .catch(err => {
            console.log(2);
            res.json(err);
        });
})

/* Update User */
users.put("/updateUser/:id", (req, res) => {
    User.findOneAndUpdate({
        _id: req.params.id
    },{
        $set: {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
        }
    },
    {
        new: true
    }
    ).then(user => res.json(user))
     .catch(err => res.json(err))
})


/* Delete a user */
users.delete("/delUserbyID/:user_id", (req, res) => {
    User.findOneAndRemove({
      _id: req.params.user_id
    })
      .then(users => res.send(`${users.first_name+users.last_name} Successfully deleted`))
      .catch(err => res.json(err));
});


module.exports = users