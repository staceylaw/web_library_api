const mongoose = require("mongoose")
const Schema = mongoose.Schema

const loanSchema = new Schema({
    book_id: {
        type: mongoose.Schema.Types.ObjectId,
         ref: 'Book'
    },
    book_title: {
        type: String,
        required: true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
         ref: 'Loan'
    },
    user_name:{
        type: String,
        required: true
    },
    loan_Issue_Date:{
        type: String,
        required: true
    },
    loan_Due_Date:{
        type: String,
        required: true
    },
    loan_Status:{
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Loan = mongoose.model('loans', loanSchema)