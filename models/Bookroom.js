const mongoose = require("mongoose")
const Schema = mongoose.Schema

const bookingSchema = new Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
         ref: 'Loan'
    },
    user_name:{
        type: String,
        required: true
    },
    booking_email: {
        type: String,
        required: true
    },
    booking_tel:{
        type: Number,
        required: true
    },
    booking_room:{
        type: String,
        required: true
    },
    booking_Usage:{
        type:String,
        required: false
    },
    booking_Date:{
        type:String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Bookroom = mongoose.model('bookrooms', bookingSchema)