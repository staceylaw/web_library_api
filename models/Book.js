const mongoose = require("mongoose")
const Schema = mongoose.Schema

const bookSchema = new Schema({
    book_title: {
        type: String,
        required: true
    },
    book_author:{
        type: String,
        required: true
    },
    book_NumOfCop: {
        type: Number,
        required: true
    },
    description:{
        type:String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Book = mongoose.model('books', bookSchema)